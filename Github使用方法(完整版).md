# GitHub 使用方法
## GitHub 是什么？
- GitHub 是一个免费代码托管平台，用于管理代码历史纪录与远程协作，可以让你和他人在任何地方共同开展项目！

![GitHub](https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1506348332308&di=8e46e56d93d477d9a2213d16e178238f&imgtype=0&src=http%3A%2F%2Fimg.sj33.cn%2Fuploads%2Fallimg%2F201304%2F1323323T6-3.png)
***
## 功能简要介绍
### 基本界面
打开 GitHub 网站 https://github.com/， 注册账号并登录，进入个人主页。

![个人主页](http://wx3.sinaimg.cn/mw690/dddbdb08gy1fjw1cia4bbj20sa0hu3zx.jpg)

页面中间的菜单栏显示了你使用 GitHub 的基本情况。

![菜单栏](http://wx2.sinaimg.cn/mw690/dddbdb08gy1fjw1ev1iuyj20fh027dfm.jpg)
![贡献情况](http://wx3.sinaimg.cn/mw690/dddbdb08gy1fjw1hp4ltoj20kw05zglq.jpg)
- Repository :存放代码的储存库,通常用于组织单个项目,存储库可以包含文件夹和文件、图像、视频、电子表格和数据集等任何你的项目所需要的内容
- Star :你收到的赞
- Follower :关注你的人
- Following :你关注的人
- Contributions :你在 GitHub 的使用或贡献情况,每个方格代表一个日期,贡献程度随颜色加深而递增

### 主要操作：
- Fork ：
将别人建立好的储存库 repository 全部复制到自己的账户中，会在自己的账户中出现同样名字的repository
- Clone ：
将 repository 复制到本地或客户端
- Roll back to this commit ：
回退到之前的版本
- Branch ：
分支，是同时对同一储存库进行编辑的方法， GitHub 储存库默认有一个主分支 master ，当我们在主分支 Master 开发过程中遇到一个新的功能需求，我们就可以新建一个分支同步开发而互不影响，开发完成后，再合并 merge 到主分支Master上
- Commits :提交,保存更改

#### GitHub Desktop 的操作
- Add ：
加入到已有的 repository 中
- Clone ：
复制到本地
- Create ：
创建新的 repository
- Publish ：
将本地的更新同步到 GitHub 中
***
## 使用步骤
掌握以下简单几步，我们就可以开始使用 GitHub 啦！

```
graph TD
创建与使用存储库-->启动与管理新分支
启动与管理新分支-->修改与提交文件
修改与提交文件-->提出与合并请求
```
***
### 1. 创建与使用存储库
- 页面右上角,在你的头像旁边找到“+”,点击并选择新的存储库 New Repository

![创建储存库](http://wx4.sinaimg.cn/mw690/dddbdb08gy1fjw2lphhvuj207x04xglh.jpg)
- 命名你的存储库
- 写一个简短的描述
- 选择以自述文件初始化 Initi a lize this Repository with a README

![创建储存库](https://guides.github.com/activities/hello-world/create-new-repo.png)
- 单击创建储存库 Create Repository
***
### 2. 启动与管理新分支

默认情况下，你的存储库有一个名为 Master 的主分支，也叫最终分支。我们使用其他分支进行实验并在提交给主分支Master之前进行编辑

当你在主分支上创建一个分支时，你在主分支的基础上复制了一个分支。如果有人在你对分支工作时对主分支进行了更改，你可以将这些更新拖进主分支，分支间的关系如下所示

![分支关系示意图](https://guides.github.com/activities/hello-world/branching.png)

*具体操作：*
- 在新建的储存库里,单击文件列表顶部的下拉框,显示主分支 master
- 在文本框内输入新分支的名称,如在 readme - edits
- 选择蓝色创建分支框或单击键盘上的“Enter”

![创建分支](https://guides.github.com/activities/hello-world/readme-edits.gif)
***
### 3. 修改与提交文件

现在，你在 readme - edits 分支的代码视图中，这是主分支的一个副本。我们开始编辑。

在 GitHub 上，保存的变化称为提交 commits 。每个提交都有一个关联的提交消息，解释为什么做出了特定更改。提交消息捕获更改的历史，因此其他贡献者可以理解您所做的工作和原因。

*具体操作：*
- 单击 readme . md 文件
- 点击位于文件预览右上角的铅笔图标,进行编辑
- 在编辑窗口内进行编辑
- 写明提交信息,描述你的修改
- 点击 Commit Changes 按钮

![修改与提交](https://guides.github.com/activities/hello-world/commit.png)

这些修改仅被保存在 readme - edits 分支，这使得它与主分支 master 有所不同。
***
### 4. 提出请求 Pull Request

由于刚刚的编辑， readme - edits 分支已经能区别于主分支 master ，我们就能提出请求（合并）。

提出请求 Pull Request 是 GitHub 协作的核心。当你提出请求时，你在提议并请求他人查看你的修改，并将修改合并入他们的分支。提出请求显示了分支之间的差异，绿色表示添加，红色表示删减。

*具体操作：*
- 单击 Pull Request 按钮,然后页面单击绿色的 New Pull Request按钮

![image](https://guides.github.com/activities/hello-world/pr-tab.gif)
- 选择你所编辑的分支,与主分支进行比较

![image](https://guides.github.com/activities/hello-world/pick-branch.png)
- 在对比页面检查分支间的差异,确保它们是你想提交的内容

![image](https://guides.github.com/activities/hello-world/diff.png)
- 当你对想要提交的修改满意时,单击绿色的 Create Pull Request 按钮

![image](https://guides.github.com/activities/hello-world/create-pr.png)
- 为你创建的 Pull Request 命名,并简要说明你做出的修改

![image](https://guides.github.com/activities/hello-world/pr-form.png)
- 确认好以上信息，单击 Create pull request 就可以啦！
***
### 5.合并请求 Pull Request

到了最后一步，是时候把你的更改放在一起啦——将你编辑的分支合并到主分支中。

*具体操作：*
- 单击绿色的合并请求 Merge Pull Request 按钮,将更改合并到主目录中
- 单击确认合并 Confirm merge
- 更改已被合并,原来编辑的分支就可以删除了,点击紫色的删除分支 Delete branch 按钮

![image](https://guides.github.com/activities/hello-world/delete-button.png)

## GitHub 与 Stata 结合
- 在 GitHub 中搜索 stata 相关信息，并 fork 到自己的账户：
1. 登录 GitHub ，在搜索框中输入关键字，如 stata ，单击回车

![image](https://ooo.0o0.ooo/2017/10/05/59d6496c7e4e5.png)

2.选择查找的内容

![image](https://i.loli.net/2017/10/05/59d6496c64776.png)

3.选择排序方式

![image](https://i.loli.net/2017/10/05/59d64a47175e9.png)


4.选择一个 repository ， fork 到自己的账户中

单击 fork ，保存到自己的账户中
![image](https://i.loli.net/2017/10/05/59d6495cd540b.png)


fork 成功的 repository 会出现在自己的账户中
![image](https://i.loli.net/2017/10/05/59d6495d8d173.png)

5.取消 fork ：

单击 setting
![image](https://ooo.0o0.ooo/2017/10/05/59d6495d8a728.png)

下拉出现“Danger Zone”，点击“Delete this repository”
![image](https://i.loli.net/2017/10/05/59d6495d8bf6c.png)

在输入框中正确输入 repository 名称，下方按钮“I understand the consequences, delete this repository”会亮起，单击即可成功取消 fork
![image](https://i.loli.net/2017/10/05/59d6495d82e04.png)

成功删除后，界面会出现提示
![image](https://i.loli.net/2017/10/05/59d6495d789f3.png)

- stata 中使用 GitHub

1.安装 GitHub :
在 stata 中输入： net install github , from ("https://raw.githubusercontent.com/haghish/github/master/")

安装成功后出现

![image](https://i.loli.net/2017/10/05/59d6496c123b0.png)

2.用 github 安装 GitHub 用户开发的命令

要安装命令包，需要 GitHub 用户名和存储库的名称。 例如，要安装 devtools ，你会发现并不能用 ssc 安装，也就是说devtools并没有被ssc收录。
![image](https://i.loli.net/2017/10/05/59d6496c2d6fb.png)

在 GitHub 中可以搜索到 gvegayon 用户开发了 devtools 。

输入以下 code 来安装 gvegayon 开发的命令：
github install gvegayon/devtools

安装时基本的语法是
github install 用户名/命令，例如：
github install haghish/findall

3.卸载安装包

要卸载命令包，请使用 uninstall 子命令，后跟程序包名称。 例如卸载刚刚安装好的 devtools 命令的安装包：
github uninstall devtools

卸载 github 本身的安装包，命令为：
ado uninstall github


**看到这里，你已经初步学会如何创建项目并进行修改与合并啦！可喜可贺！继续开始新的探索吧！**




***
参考网址：

[Hello World GitHub Guides](https://guides.github.com/activities/hello-world/)

[ GitHub for Windows使用教程](http://youngxhui.github.io/2016/05/03/GitHub-for-Windows%E4%BD%BF%E7%94%A8%E6%95%99%E7%A8%8B(%E4%B8%80)/)

[怎样使用 GitHub？](https://www.zhihu.com/question/20070065)

[GitHub使用](http://www.cnblogs.com/yanliujun-tangxianjun/)

[SSC的好兄弟“github”](http://www.sohu.com/a/160488855_697896)
